# Listing of Parameters
# ---------------------
subsection DG Parameters
  # The penalty parameter for enforcing Dirichlet boundary conditions in DG
  set Boundary Term Penalty = 1.0e+10

  # The penalty parameter for face terms in DG
  set Face Term Penalty     = 1.0e+14

  # true results in scaling of face penalty at internal edges
  set Scale Penalty         = false

  # The sform determines which class of DG method will be used
  # (-1:SIPG, 0:IIPG, +1:NIPG, OBB)
  # Note: SIPG is unstable and sensitive to changes in Penalty
  set sform                 = 1
end


subsection Elasticity Parameters
  # The elastic energy restitution rate for crack growth [N/mm]
  set Critical Fracture Energy = 2.7

  # Lame's First Parameter [MPa]
  set Lame's Parameter Lambda  = 121.15e+3

  # Lame's Second Parameter a.k.a Shear Modulus [MPa]
  set Lame's Parameter Mu      = 80.77e+3
end


subsection Mesh Parameters
  # Number of global refines prior to local refinement
  set Num Global Refines          = 3

  # true/false to enable/disable P-C mesh adaptivity
  set Predictor Corrector         = true

  # true for a-priori refinement in the case of MS/MT tests
  set Refine Apriori              = false

  # Value of PF for which a cell will be refined
  set Threshold PF for Refinement = 0.8
end


subsection Phase-Field Parameters
  # Space regularization constant Epsilon [mm]
  # Controls width of cracked PF zone
  # Should be chosen ~ O(h)
  set Constant E            = 4.4e-2

  # Energy regularization constant Kappa
  # Can theoretically be chosen to be zero
  set Constant K            = 1e-12

  # true to enable auto-determination of kappa and epsilon from mesh size
  set Dynamic PF Parameters = false

  # Specify how the semi-linear form is linearized
  # Note: interpolation is not stable with discontinuous methods
  set Linearization Scheme  = simple
end


subsection Runtime Parameters
  # True/Non-zero results in spectral decomposition of stress tensor in the
  # matrix
  set Decompose Stress Matrix = false

  # True/Non-zero results in spectral decomposition of stress tensor in the
  # RHS
  set Decompose Stress RHS    = false

  # [DISABLED: Use maxtime] Number of timesteps to take
  set Maximum Timesteps       = 350

  # Generate vtk output every N steps
  set Output Skip             = 1

  # True causes timestep to adapt to 1e-5 near rupture
  set Quick Test              = false

  # Maxtime to run the problem till
  set Simulation End Time     = 6.5e-3

  # Name of the test case that is to be run
  set Test Case               = miehe tension

  # Timestep size [s]
  set Timestep Size           = 5e-5
end


subsection Solver Parameters
  # Tolerance of the outer loop Augmented Lagrange iterations
  set AugLag Tolerance           = 1.0e-4

  # Maximum allowable Newton Iterations at a given level
  set Maximum Newton Iterations  = 50

  # Maximum allowable augmented lagrange iterations at a given timestep
  set Maximum Penalty Iterations = 500

  # Tolerance of the newton iterations goes here
  set Newton Tolerance           = 5.0e-8

  # Type of outer-loop optimization to be employed
  set Optimization Type          = simple penalization

  # Constant penalty term from outer loop PF-constraint
  set Penalty Gamma              = 1e-15

  # True results in cutting of the timestep in the event of non-convergence
  set Timestep Cutting           = false
end


