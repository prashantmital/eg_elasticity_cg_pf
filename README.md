Phase-Field Models of Fracture
==============================

This repo contains code that models fractures in linearly elastic
materials using a phase-field technique. The version is fully
compatible with deal.II-8.1.0 and may/may not run on more recent
releases of the library. The deal.II binaries used to run this code
were built using gcc/4.8 compilers. 

For more information please contact: prashantmital@gmail.com

TODO:
1. Use CG for PhaseField (currently DG is being used)
