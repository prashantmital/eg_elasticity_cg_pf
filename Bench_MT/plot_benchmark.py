import matplotlib.pyplot as plt
import numpy as np

def populate_refdata(refcase):
    with open(refcase, 'r') as f_read:
        raw_refdata = f_read.readlines()
        refdata = np.asarray([
            [float(x[3])for x in [data.split() for data in raw_refdata]],
            [float(x[12])for x in [data.split() for data in raw_refdata]]
            ], 'float64')
        return refdata


def filter_OBB(testdata):
    # quick and dirty way of doing this:
    # first ensure you have climbed above 
    # some value. here I take 700. then when
    # the series goes below 700, only keep the
    # monotonous data (if force goes UP
    # beyond this point, we know we are in the
    # oscillatory regime).
    counter = 0
    dsize = len(testdata[1,:])
    for i in range(dsize):
        if (testdata[1, i] > 700):
            counter = i
            break
        else:
            continue

    for i in range(counter, dsize):
        if (testdata[1,i] < 700):
            counter = i
            break
        else:
            continue

    for i in range(counter, dsize):
        if (testdata[1,i+1] > testdata[1,i]):
            counter = i
            break
        else:
            continue

    return testdata[:,:counter]


def get_testdata(testcase):
    with open(testcase, 'r') as f_read:
        _testdata_uy = list()
        _testdata_Fy = list()
        for lines in f_read:
            try:
                line = lines.split()
                if (line[0] == "Fy:"):
                    _testdata_uy.append(float(line[1]))
                    _testdata_Fy.append(float(line[2]))
            except IndexError:
                continue
            
    testdata = np.asarray([_testdata_uy,_testdata_Fy], 'float64')
    return testdata


def plot(method, case_name, file_name, refcase): 
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    
    refdata = populate_refdata(refcase)
    plt.plot(refdata[0, :], refdata[1, :], label="Reference Data", linewidth=1.25)

    for (name, src) in zip(case_name, file_name):
        testdata = get_testdata(src)
        kwargs = {"label":name, "linewidth":1.25}
        if name is "OBB":
            #can be used to customize output for specific cases
            #testdata = filter_OBB(testdata)
            args = (testdata[0,:], testdata[1, :])
        else:
            args = (testdata[0,:], testdata[1, :])
        plt.plot(*args, **kwargs)

    plt.legend(loc=2)
    plt.xlabel(r'Displacement $u_y$ [mm]')
    plt.ylabel(r'Force $F_y$ [N]')
    title = 'Miehe Tension Test [{}]'.format(method)
    plt.title(title, fontsize=16)
    plt.grid(True)
    plt.savefig(method)
    plt.show()
    
    
if __name__=='__main__':
    method = "EG"
    start = 4
    end = 4

    #file_name = list()
    #for x in range(start, end+1):
    #    file_name.append(method+"_id_"+str(x)+".out")
    
    file_name = ["screenout_miehe.out"]
    case_name = ["Enriched Galerkin"]
    
    refcase = "datei_functionals_miehe_tension_ref_adapt_4_1.txt"
    plot(method, case_name, file_name, refcase)


