///////////////////////////////////////////////////////////
// Initial boundary conditions


          // fix y component of left and right bottom corners
          typename DoFHandler<dim>::active_cell_iterator cell =
            dof_handler.begin_active(), endc = dof_handler.end();

          for (; cell != endc; ++cell)
            {
              if (cell->is_artificial())
                continue;


              for (unsigned int v = 0;
                   v < GeometryInfo<dim>::vertices_per_cell; ++v)
                {
                  if (
                    std::abs(cell->vertex(v)[1]) < 1e-10
                    &&
                    (
                      std::abs(cell->vertex(v)[0]+4.0) < 1e-10
                      || std::abs(cell->vertex(v)[0]-4.0) < 1e-10
                    ))
                    {
                      types::global_dof_index idx = cell->vertex_dof_index(v, introspection.component_indices.displacement[1]);// y displacement
                      boundary_values[idx] = 0.0;
                      idx = cell->vertex_dof_index(v, introspection.component_indices.displacement[0]);// x displacement
                      if (std::abs(cell->vertex(v)[0]+4.0) < 1e-10)
                        boundary_values[idx] = 0.0;
                    }
                  else if (
                    std::abs(cell->vertex(v)[0]) < 1e-10
                    &&
                    std::abs(cell->vertex(v)[1]-2.0) < 1e-10
                  )
                    {
                      types::global_dof_index idx = cell->vertex_dof_index(v, introspection.component_indices.displacement[0]);// x displacement
                      //boundary_values[idx] = 0.0;
                      idx = cell->vertex_dof_index(v, introspection.component_indices.displacement[1]);// y displacement
                      boundary_values[idx] = -1.0*time;
                    }

                }
            }

        }


///////////////////////////////////////////////////////////
// Newton boundary conditions

         // fix y component of left and right bottom corners
          typename DoFHandler<dim>::active_cell_iterator cell =
            dof_handler.begin_active(), endc = dof_handler.end();

          for (; cell != endc; ++cell)
            {
              if (cell->is_artificial())
                continue;

              for (unsigned int v = 0;
                   v < GeometryInfo<dim>::vertices_per_cell; ++v)
                {
                  if (
                    std::abs(cell->vertex(v)[1]) < 1e-10
                    &&
                    (
                      std::abs(cell->vertex(v)[0]+4.0) < 1e-10
                      || std::abs(cell->vertex(v)[0]-4.0) < 1e-10
                    ))
                    {

                      types::global_dof_index idx = cell->vertex_dof_index(v, 1);// 1=y displacement
                      constraints_update.add_line(idx);
                      idx = cell->vertex_dof_index(v, 0);// 0=x displacement
                      if (std::abs(cell->vertex(v)[0]+4.0) < 1e-10)
                        constraints_update.add_line(idx);
                    }
                  else if (
                    std::abs(cell->vertex(v)[0]) < 1e-10
                    &&
                    std::abs(cell->vertex(v)[1]-2.0) < 1e-10
                  )
                    {
                      types::global_dof_index idx = cell->vertex_dof_index(v, 0);// 0=x displacement
                      //constraints_update.add_line(idx);
                      idx = cell->vertex_dof_index(v, 1);// 1=y displacement
                      constraints_update.add_line(idx);
                    }
                }
            }

        }
