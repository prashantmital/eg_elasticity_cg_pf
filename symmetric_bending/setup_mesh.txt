template <int dim>
void
FracturePhaseFieldProblem<dim>::setup_mesh(Triangulation<dim> &tria)
{
  std::string mesh_info;

  switch (test_case)
    {
    case TestCase::miehe_shear:
    case TestCase::miehe_tension:
      mesh_info = "ucd meshes/unit_slit.inp";
      break;

    case TestCase::miehe_shear_eps_slit:
      mesh_info = "rect 0 0 1 1";
      break;

    case TestCase::long_tension:
      mesh_info = "ucd meshes/unit_slit_double.inp";
      //mesh_info = "ucd meshes/unit_slit_three.inp";
      break;

    case TestCase::borden_tension:
      mesh_info = "ucd meshes/Borden_slit_2.inp";
      break;

    case TestCase::sneddon:
      if (dim==2)
        mesh_info = "rect -10 -10 10 10";
      else
        mesh_info = "rect -10 -10 -10 10 10 10";
      break;

    case TestCase::multiple_homo:
    case TestCase::multiple_het:
      if (dim==2)
        mesh_info = "ucd meshes/unit_square_4.inp";
      else
        mesh_info = "ucd meshes/unit_cube_10.inp";
      break;

    case TestCase::three_point_bending:
      //mesh_info = "msh meshes/threepoint-notsym_b.msh";
      mesh_info = "msh meshes/threepoint-notsym.msh";
      //mesh_info = "msh meshes/threepoint.msh";
      break;

    case TestCase::stationary_slit:
      mesh_info = "rect -1 -1 1 1";
      break;
    }



  // overwrite defaults from parameter file if given
  if (mesh != "")
    mesh_info = mesh;

  AssertThrow(mesh_info!="", ExcMessage("Error: no mesh information given."));

  std::istringstream is(mesh_info);
  std::string type;
  std::string grid_name = "";
  typename GridIn<dim>::Format format = GridIn<dim>::ucd;
  is >> type;

  if (type=="rect")
    {
      Point<dim> p1, p2;
      if (dim==2)
        is >> p1[0] >> p1[1] >> p2[0] >> p2[1];
      else
        is >> p1[0] >> p1[1] >> p1[2] >> p2[0] >> p2[1] >> p2[2];

      GridGenerator::hyper_rectangle(tria,
                                     p1,
                                     p2,
                                     true);
    }
  else if (type=="msh")
    {
      format = GridIn<dim>::msh;
      is >> grid_name;
    }
  else if (type=="ucd")
    {
      format = GridIn<dim>::ucd;
      is >> grid_name;
    }

  if (grid_name != "")
    {
      GridIn<dim> grid_in;
      grid_in.attach_triangulation(tria);
      std::ifstream input_file(grid_name.c_str());

      grid_in.read(input_file, format);
    }

  //GridTools::distort_random(0.35, triangulation);

  if (test_case == TestCase::three_point_bending)
    {
      double eps_machine = 1.0e-10;

      typename Triangulation<dim>::active_cell_iterator
      cell = tria.begin_active(),
      endc = tria.end();
      for (; cell!=endc; ++cell)
        for (unsigned int f=0;
             f < GeometryInfo<dim>::faces_per_cell;
             ++f)
          {
            const Point<dim> face_center = cell->face(f)->center();
            if (cell->face(f)->at_boundary())
              {
                if ((face_center[1] < 2.0+eps_machine) && (face_center[1] > 2.0-eps_machine)
                    //&& (face_center[0] < 0.000000001) && (face_center[0] > -3.9999999999)
                   )
                  compatibility::set_boundary_indicator(cell->face(f), 3);
                else if ((face_center[0] < -4.0+eps_machine) && (face_center[0] > -4.0-eps_machine)
                         //&& (face_center[0] < 0.000000001) && (face_center[0] > -3.9999999999)
                        )
                  compatibility::set_boundary_indicator(cell->face(f), 0);
                else if ((face_center[0] < 4.0+eps_machine) && (face_center[0] > 4.0-eps_machine)
                         //&& (face_center[0] < 0.000000001) && (face_center[0] > -3.9999999999)
                        )
                  compatibility::set_boundary_indicator(cell->face(f), 1);
              }
          }
    }
}
